Please refer to the PolyHideInstructions.pdf document that has been copied into your macro folder.
You can access this document by selecting S9 from the macro main menu (you will require Adobe Reader [or similar] software to read it).

DataCAD 14 introduced polygons with up to 256 sides: This version of the macro is limited to processing polygons with no more than 36 sides. 
The macro will display a warning if it encounters polygons with between 37 and 40 sides, but it may crash your drawing without warning if you have polygons with more than 40 sides (tested in DataCAD version 19.01.00.14). This is a bug in DataCAD and the DCAL programming language and there is nothing I could do to avoid it at the time of writing this macro. (I hope to re-write this macro using DCAL for Delphi at some stage, and it should then be possible to fix this problem.)

If you have polygons with more than 36 sides then the workaround is to use the �Save As� command to save your drawing back to DataCAD 13 or earlier format. When exported back to DataCAD 13 format Polygons and Slabs with more than 36 vertices are exploded to a collection of three-sided polygons which can be processed by this (and other) macros.

Bug reports or enhancement requests can be sent to dhsoftware1@gmail.com

Thank you,
David Henderson
dhsoftware1@gmail.com
www.dhsoftware.com.au